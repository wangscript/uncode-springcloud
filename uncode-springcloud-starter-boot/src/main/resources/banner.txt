${AnsiColor.BRIGHT_CYAN}██╗   ██╗███╗   ██╗ ██████╗ ██████╗ ██████╗ ███████╗     ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
${AnsiColor.BRIGHT_CYAN}██║   ██║████╗  ██║██╔════╝██╔═══██╗██╔══██╗██╔════╝    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
${AnsiColor.BRIGHT_CYAN}██║   ██║██╔██╗ ██║██║     ██║   ██║██║  ██║█████╗      ██║     ██║     ██║   ██║██║   ██║██║  ██║
${AnsiColor.BRIGHT_CYAN}██║   ██║██║╚██╗██║██║     ██║   ██║██║  ██║██╔══╝      ██║     ██║     ██║   ██║██║   ██║██║  ██║
${AnsiColor.BRIGHT_CYAN}╚██████╔╝██║ ╚████║╚██████╗╚██████╔╝██████╔╝███████╗    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
${AnsiColor.BRIGHT_CYAN} ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚═════╝ ╚═════╝ ╚══════╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

${AnsiColor.BLUE}:: Uncode :: ${spring.application.name}:${AnsiColor.RED}${info.app.env}:${AnsiColor.RED}${info.app.version}${AnsiColor.BLUE} :: Running SpringBoot ${spring-boot.version} :: ${AnsiColor.BRIGHT_BLACK}