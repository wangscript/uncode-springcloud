package cn.uncode.springcloud.utils.obj;

import org.springframework.lang.Nullable;

/**
 * 对象工具类
 *
 * @author Juny
 */
public final class ObjectUtil extends org.springframework.util.ObjectUtils {
	
	private ObjectUtil() {}

	/**
	 * 判断元素不为空
	 * @param obj object
	 * @return boolean
	 */
	public static boolean isNotEmpty(@Nullable Object obj) {
		return !ObjectUtil.isEmpty(obj);
	}

}
