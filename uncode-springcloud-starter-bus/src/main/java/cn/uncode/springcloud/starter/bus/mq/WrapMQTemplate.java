package cn.uncode.springcloud.starter.bus.mq;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.ReceiveAndReplyCallback;
import org.springframework.amqp.core.ReplyToAddressCallback;
import org.springframework.core.ParameterizedTypeReference;

public class WrapMQTemplate implements AmqpTemplate {
	
	private AmqpTemplate amqpTemplate;
	
	public WrapMQTemplate(AmqpTemplate amqpTemplate) {
		this.amqpTemplate = amqpTemplate;
	}

	@Override
	public void send(Message message) throws AmqpException {
		amqpTemplate.send(message);
	}

	@Override
	public void send(String routingKey, Message message) throws AmqpException {
		amqpTemplate.send(routingKey, message);
	}

	@Override
	public void send(String exchange, String routingKey, Message message) throws AmqpException {
		amqpTemplate.send(exchange, routingKey, message);
	}

	@Override
	public void convertAndSend(Object message) throws AmqpException {
		amqpTemplate.convertAndSend(message);

	}

	@Override
	public void convertAndSend(String routingKey, Object message) throws AmqpException {
		amqpTemplate.convertAndSend(routingKey, message);
	}

	@Override
	public void convertAndSend(String exchange, String routingKey, Object message) throws AmqpException {
		amqpTemplate.convertAndSend(exchange, routingKey, message);
	}

	@Override
	public void convertAndSend(Object message, MessagePostProcessor messagePostProcessor) throws AmqpException {
		amqpTemplate.convertAndSend(message, messagePostProcessor);
	}

	@Override
	public void convertAndSend(String routingKey, Object message, MessagePostProcessor messagePostProcessor)
			throws AmqpException {
		amqpTemplate.convertAndSend(routingKey, message, messagePostProcessor);
	}

	@Override
	public void convertAndSend(String exchange, String routingKey, Object message,
			MessagePostProcessor messagePostProcessor) throws AmqpException {
		amqpTemplate.convertAndSend(exchange, routingKey, message, messagePostProcessor);
	}

	@Override
	public Message receive() throws AmqpException {
		return amqpTemplate.receive();
	}

	@Override
	public Message receive(String queueName) throws AmqpException {
		return amqpTemplate.receive(queueName);
	}

	@Override
	public Message receive(long timeoutMillis) throws AmqpException {
		return amqpTemplate.receive(timeoutMillis);
	}

	@Override
	public Message receive(String queueName, long timeoutMillis) throws AmqpException {
		return amqpTemplate.receive(queueName, timeoutMillis);
	}

	@Override
	public Object receiveAndConvert() throws AmqpException {
		return amqpTemplate.receiveAndConvert();
	}

	@Override
	public Object receiveAndConvert(String queueName) throws AmqpException {
		return amqpTemplate.receiveAndConvert(queueName);
	}

	@Override
	public Object receiveAndConvert(long timeoutMillis) throws AmqpException {
		return amqpTemplate.receiveAndConvert(timeoutMillis);
	}

	@Override
	public Object receiveAndConvert(String queueName, long timeoutMillis) throws AmqpException {
		return amqpTemplate.receiveAndConvert(queueName, timeoutMillis);
	}

	@Override
	public <T> T receiveAndConvert(ParameterizedTypeReference<T> type) throws AmqpException {
		return amqpTemplate.receiveAndConvert(type);
	}

	@Override
	public <T> T receiveAndConvert(String queueName, ParameterizedTypeReference<T> type) throws AmqpException {
		return amqpTemplate.receiveAndConvert(queueName, type);
	}

	@Override
	public <T> T receiveAndConvert(long timeoutMillis, ParameterizedTypeReference<T> type) throws AmqpException {
		return amqpTemplate.receiveAndConvert(timeoutMillis, type);
	}

	@Override
	public <T> T receiveAndConvert(String queueName, long timeoutMillis, ParameterizedTypeReference<T> type)
			throws AmqpException {
		return amqpTemplate.receiveAndConvert(queueName, timeoutMillis, type);
	}

	@Override
	public <R, S> boolean receiveAndReply(ReceiveAndReplyCallback<R, S> callback) throws AmqpException {
		return amqpTemplate.receiveAndReply(callback);
	}

	@Override
	public <R, S> boolean receiveAndReply(String queueName, ReceiveAndReplyCallback<R, S> callback)
			throws AmqpException {
		return amqpTemplate.receiveAndReply(queueName, callback);
	}

	@Override
	public <R, S> boolean receiveAndReply(ReceiveAndReplyCallback<R, S> callback, String replyExchange,
			String replyRoutingKey) throws AmqpException {
		return amqpTemplate.receiveAndReply(callback, replyExchange, replyRoutingKey);
	}

	@Override
	public <R, S> boolean receiveAndReply(String queueName, ReceiveAndReplyCallback<R, S> callback,
			String replyExchange, String replyRoutingKey) throws AmqpException {
		return amqpTemplate.receiveAndReply(queueName, callback, replyExchange, replyRoutingKey);
	}

	@Override
	public <R, S> boolean receiveAndReply(ReceiveAndReplyCallback<R, S> callback,
			ReplyToAddressCallback<S> replyToAddressCallback) throws AmqpException {
		return amqpTemplate.receiveAndReply(callback, replyToAddressCallback);
	}

	@Override
	public <R, S> boolean receiveAndReply(String queueName, ReceiveAndReplyCallback<R, S> callback,
			ReplyToAddressCallback<S> replyToAddressCallback) throws AmqpException {
		return amqpTemplate.receiveAndReply(queueName, callback, replyToAddressCallback);
	}

	@Override
	public Message sendAndReceive(Message message) throws AmqpException {
		return amqpTemplate.sendAndReceive(message);
	}

	@Override
	public Message sendAndReceive(String routingKey, Message message) throws AmqpException {
		return amqpTemplate.sendAndReceive(routingKey, message);
	}

	@Override
	public Message sendAndReceive(String exchange, String routingKey, Message message) throws AmqpException {
		return amqpTemplate.sendAndReceive(exchange, routingKey, message);
	}

	@Override
	public Object convertSendAndReceive(Object message) throws AmqpException {
		return amqpTemplate.convertSendAndReceive(message);
	}

	@Override
	public Object convertSendAndReceive(String routingKey, Object message) throws AmqpException {
		return amqpTemplate.convertSendAndReceive(routingKey, message);
	}

	@Override
	public Object convertSendAndReceive(String exchange, String routingKey, Object message) throws AmqpException {
		return amqpTemplate.convertSendAndReceive(exchange, routingKey, message);
	}

	@Override
	public Object convertSendAndReceive(Object message, MessagePostProcessor messagePostProcessor)
			throws AmqpException {
		return amqpTemplate.convertSendAndReceive(message, messagePostProcessor);
	}

	@Override
	public Object convertSendAndReceive(String routingKey, Object message, MessagePostProcessor messagePostProcessor)
			throws AmqpException {
		return amqpTemplate.convertSendAndReceive(routingKey, message, messagePostProcessor);
	}

	@Override
	public Object convertSendAndReceive(String exchange, String routingKey, Object message,
			MessagePostProcessor messagePostProcessor) throws AmqpException {
		return amqpTemplate.convertSendAndReceive(exchange, routingKey, message, messagePostProcessor);
	}

	@Override
	public <T> T convertSendAndReceiveAsType(Object message, ParameterizedTypeReference<T> responseType)
			throws AmqpException {
		return amqpTemplate.convertSendAndReceiveAsType(message, responseType);
	}

	@Override
	public <T> T convertSendAndReceiveAsType(String routingKey, Object message,
			ParameterizedTypeReference<T> responseType) throws AmqpException {
		return amqpTemplate.convertSendAndReceiveAsType(routingKey, message, responseType);
	}

	@Override
	public <T> T convertSendAndReceiveAsType(String exchange, String routingKey, Object message,
			ParameterizedTypeReference<T> responseType) throws AmqpException {
		return amqpTemplate.convertSendAndReceiveAsType(exchange, routingKey, message, responseType);
	}

	@Override
	public <T> T convertSendAndReceiveAsType(Object message, MessagePostProcessor messagePostProcessor,
			ParameterizedTypeReference<T> responseType) throws AmqpException {
		return amqpTemplate.convertSendAndReceiveAsType(message, messagePostProcessor, responseType);
	}

	@Override
	public <T> T convertSendAndReceiveAsType(String routingKey, Object message,
			MessagePostProcessor messagePostProcessor, ParameterizedTypeReference<T> responseType)
			throws AmqpException {
		return amqpTemplate.convertSendAndReceiveAsType(routingKey, message, messagePostProcessor, responseType);
	}

	@Override
	public <T> T convertSendAndReceiveAsType(String exchange, String routingKey, Object message,
			MessagePostProcessor messagePostProcessor, ParameterizedTypeReference<T> responseType)
			throws AmqpException {
		return amqpTemplate.convertSendAndReceiveAsType(exchange, routingKey, message, messagePostProcessor, responseType);
	}

}
