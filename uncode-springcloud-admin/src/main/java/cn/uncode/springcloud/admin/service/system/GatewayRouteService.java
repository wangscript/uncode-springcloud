package cn.uncode.springcloud.admin.service.system;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.uncode.dal.criteria.QueryCriteria;
import cn.uncode.springcloud.admin.common.AppConstants;
import cn.uncode.springcloud.admin.dal.system.ConfigDAL;
import cn.uncode.springcloud.admin.dal.system.GatewayRouteDAL;
import cn.uncode.springcloud.admin.model.system.dto.ConfigDTO;
import cn.uncode.springcloud.admin.model.system.dto.GatewayRouteDTO;
import cn.uncode.springcloud.starter.web.result.P;

@Service
public class GatewayRouteService {

    @Autowired 
    private GatewayRouteDAL gatewayRouteDAL;
    
    @Autowired
    private ConfigDAL configDAL;

    public long add(GatewayRouteDTO route) {
    	Date time = new Date();
    	route.setCreateTime(time);
    	route.setUpdateTime(time);
    	route.setStatus(AppConstants.GATEWAY_ROUTE_STATUS_OK);
        return (Long) gatewayRouteDAL.insertEntity(route);
    }

    public int update(GatewayRouteDTO route) {
        route.setUpdateTime(new Date());
        route.setStatus(AppConstants.GATEWAY_ROUTE_STATUS_UPDATE);
        return gatewayRouteDAL.updateEntityById(route);
    }

    public int delete(List<Long> ids) {
    	GatewayRouteDTO dto = new GatewayRouteDTO();
    	dto.setStatus(AppConstants.GATEWAY_ROUTE_STATUS_DELETE);
    	QueryCriteria queryCriteria = new QueryCriteria();
		queryCriteria.createCriteria().andColumnIn(GatewayRouteDTO.ID, ids);
        return gatewayRouteDAL.updateEntityByCriteria(dto, queryCriteria);
    }

    public GatewayRouteDTO getById(Long id) {
        return gatewayRouteDAL.getEntityById(id);
    }

    /**
     * 查询所有路由信息
     * @return
     */
    public List<GatewayRouteDTO> getRoutes(Date time) {
    	QueryCriteria queryCriteria = new QueryCriteria();
		queryCriteria.setRecordIndex(0);
		queryCriteria.setLimit(1000);
//		queryCriteria.setTable(GatewayRouteDTO.class);
		queryCriteria.createCriteria().andColumnGreaterThanOrEqualTo(GatewayRouteDTO.UPDATE_TIME, time);
		List<GatewayRouteDTO> list = gatewayRouteDAL.getListByCriteria(queryCriteria);
        return list;
    }
    
    public P<List<GatewayRouteDTO>> getPageList(int pageIndex, int pageSize) {
    	QueryCriteria queryCriteria = new QueryCriteria();
    	if(pageIndex > 0) {
    		queryCriteria.setPageIndex(pageIndex);
    	}
    	if(pageSize > 0) {
    		queryCriteria.setPageSize(pageSize);
    	}
		queryCriteria.createCriteria().andColumnNotEqualTo(GatewayRouteDTO.STATUS, AppConstants.GATEWAY_ROUTE_STATUS_DELETE);
		Map<String, Object> resultMap = gatewayRouteDAL.getPageByCriteria4Map(queryCriteria);
		return P.valueOfDAL(resultMap, GatewayRouteDTO.class);
    }

    
    public void publish(long time) {
    	ConfigDTO dto = configDAL.getEntityById(1);
    	dto.setUpdateTime(new Date());
    	dto.setValue(dto.getValue()+","+time);
    	dto.setKey(null);
    	dto.setName(null);
    	dto.setCreateTime(null);
    	dto.setRemark(null);
    	configDAL.updateEntityById(dto);
    }
    
    public long getLastVersion() {
    	ConfigDTO dto = configDAL.getEntityById(1);
    	if(dto != null && StringUtils.isNotBlank(dto.getValue())) {
    		String[] array = dto.getValue().split(",");
    		return Long.valueOf(array[array.length-1]);
    	}
    	return 0;
    }
}
