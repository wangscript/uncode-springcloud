package cn.uncode.springcloud.admin.common;

import javax.servlet.http.HttpServletResponse;

import cn.uncode.springcloud.starter.web.result.ResultCode;

public class AdminResultCodeNO {
	
	/**
	 * 返回结果默认key名称
	 */
	public static final String RESULT_CODE_KEY = "code";
	public static final String RESULT_MESSAGE_KEY = "message";
	public static final String RESULT_DATA_KEY = "data";
	public static final String RESULT_SUCCESS_KEY = "success";
	
	
	public static final ResultCode MSG_CODE_IS_NULL = new ResultCode(HttpServletResponse.SC_FOUND, "NULL");
	/**
	 * 操作成功
	 */
	public static final ResultCode MSG_CODE_SUCCESS = new ResultCode(HttpServletResponse.SC_OK, "操作成功");
	
	public static final ResultCode MSG_CODE_SUCCESS_NO_CONTENT = new ResultCode(HttpServletResponse.SC_NO_CONTENT, "操作成功");
	/**
	 * 业务异常
	 */
	public static final ResultCode MSG_CODE_FAILURE = new ResultCode(HttpServletResponse.SC_BAD_REQUEST, "业务异常");
    /**
     * 参数校验失败
     */
    public static final ResultCode MSG_CODE_PARAM_VALID_ERROR = new ResultCode(HttpServletResponse.SC_BAD_REQUEST, "参数校验失败");
	/**
	 * 请求未授权
	 */
	public static final ResultCode MSG_CODE_UN_AUTHORIZED = new ResultCode(HttpServletResponse.SC_UNAUTHORIZED, "请求未授权");
	/**
	 * 404 没找到请求
	 */
	public static final ResultCode MSG_CODE_NOT_FOUND = new ResultCode(HttpServletResponse.SC_NOT_FOUND, "404 没找到请求%name%");
	/**
	 * 消息不能读取
	 */
	public static final ResultCode MSG_CODE_MSG_NOT_READABLE = new ResultCode(HttpServletResponse.SC_BAD_REQUEST, "消息不能读取");
	/**
	 * 不支持当前请求方法
	 */
	public static final ResultCode MSG_CODE_METHOD_NOT_SUPPORTED = new ResultCode(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "不支持当前请求方法");
	/**
	 * 不支持当前媒体类型
	 */
	public static final ResultCode MSG_CODE_MEDIA_TYPE_NOT_SUPPORTED = new ResultCode(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "不支持当前媒体类型");
	/**
	 * 请求被拒绝
	 */
	public static final ResultCode MSG_CODE_REQ_REJECT = new ResultCode(HttpServletResponse.SC_FORBIDDEN, "请求被拒绝");
	/**
	 * 服务器异常
	 */
	public static final ResultCode MSG_CODE_INTERNAL_SERVER_ERROR = new ResultCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "服务器异常");
	/**
	 * 缺少必要的请求参数
	 */
	public static final ResultCode MSG_CODE_PARAM_MISS = new ResultCode(HttpServletResponse.SC_BAD_REQUEST, "缺少必要的请求参数");
	/**
	 * 请求参数类型错误
	 */
	public static final ResultCode MSG_CODE_PARAM_TYPE_ERROR = new ResultCode(HttpServletResponse.SC_BAD_REQUEST, "请求参数类型错误");
	/**
	 * 请求参数绑定错误
	 */
	public static final ResultCode MSG_CODE_PARAM_ERROR = new ResultCode(HttpServletResponse.SC_BAD_REQUEST, "请求参数绑定错误");
    
    

}
